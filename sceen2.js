import React, {Component} from 'react';
import {ScrollView, StyleSheet, Text, View} from 'react-native';

type Props = {};
export default class Sceen2 extends Component<Props>{
    render() {
        return (
            <View style={styles.containers}>
            <View style={styles.column}>
            <View style={styles.footer}>
            <Text style={styles.footerText}>Test1</Text>
            </View>
            <View style={styles.footer2}>
            <Text style={styles.footerText}>Text</Text>
            </View>
            <View style={styles.footer}>
            <Text style={styles.footerText}>X</Text>
            </View>
            </View>

           
            
            <View style={styles.content}>
            <Text style={styles.textContent}>ScrollView</Text>
            </View>
           


            <View style={styles.column}>
            <View style={styles.footer}>
            <Text style={styles.footerText}>I</Text>
            </View>
            <View style={styles.footer}>
            <Text style={styles.footerText}>C</Text>
            </View>
            <View style={styles.footer}>
            <Text style={styles.footerText}>O</Text>
            </View>
            <View style={styles.footer}>
            <Text style={styles.footerText}>N</Text>
            </View>
            </View>
            


           </View>
        )
    }
}
const styles = StyleSheet.create({
    containers: {
        backgroundColor: 'white',
        flex:1
    },
    column: {
        flexDirection: 'row'
    },
    header: {
        backgroundColor: 'pink',
        flex: 1
    },
    content: {
        backgroundColor: 'gray',
        flex: 3,
        alignItems: 'center',
        justifyContent: 'center'
        
    },
    footer: {
        backgroundColor: 'pink',
        flex: 1,
        padding: 16,
        margin: 2,
        alignItems: 'center'
        
    },
    footer2: {
        backgroundColor: 'pink',
        flex: 2,
        padding: 16,
        margin: 2,
        alignItems: 'center'
    },
    footerText: {
        fontSize: 22,
        color: 'white',
        alignItems: 'center'
    },
    textContent: {
        fontSize: 22,
        color: 'white',
        alignItems: 'center',
        justifyContent: 'center'
    }

  
})