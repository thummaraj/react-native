import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';

type Props = {};
export default class App2 extends Component<Props>{
    render(){
        return(
            <View style={styles.containers}> 
            
            <View style={[styles.layer1, styles.center]}>
            <View style={[styles.circle, styles.center]}>
            <Text style={styles.circleText}>Image</Text>
            </View>
            </View>
            
           
            <View style={styles.layer2}>
            <View style={[styles.rectangle,styles.center]}>
            <Text style={styles.rectangleText}>Input1</Text>
            </View>
            <View style={[styles.rectangle,styles.center]}>
            <Text style={styles.rectangleText}>Input2</Text>
            </View>
            </View>


            <View style={[styles.rectangle1,styles.center]}>
            <Text style={styles.rectangleText2}>Login</Text>
            </View>


            </View>

           
            
        )
    }
}
const styles = StyleSheet.create({
    containers: {
        backgroundColor: 'gray', 
        flex: 1,
        
    }, 
    center: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    circle: {
        width: 100,
        height: 100,
        borderRadius: 100/2,
        backgroundColor: 'white',
    },
    circleText: {
        color: 'black',
       
    },
    layer1: {
        
        flex: 1,
        
        
    },
    layer2: {
        
        flex: 1,
        
    },
    rectangle: {
        backgroundColor: 'white',
        padding: 20,
        margin: 14
    
    },
    rectangle1: {
        backgroundColor: 'black',
        padding: 20,
        margin: 14
    
    },
    rectangleText: {
        color: 'black',
    },
    rectangleText2: {
        color: 'white',
        
        

    },
  
})